import { controls } from '../../constants/controls';


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    firstFighter.originalHealth = firstFighter.health
    secondFighter.originalHealth = secondFighter.health
    let keyMap = {};
    const processFight = (ev) => {
      keyMap[ev.code] = ev.type == "keydown";
  
      if (keyMap[controls.PlayerOneAttack]) {
        if (!firstFighter.isBlocking)
          if (attack(firstFighter, secondFighter))
            updateHealthBar(secondFighter, 'right');
      }
      if (keyMap[controls.PlayerOneCriticalHitCombination[0]]
        && keyMap[controls.PlayerOneCriticalHitCombination[1]]
        && keyMap[controls.PlayerOneCriticalHitCombination[2]]) {
        if (!firstFighter.isBlocking)
          if (superAttack(firstFighter, secondFighter))
            updateHealthBar(secondFighter, 'right');
      }
      if (keyMap[controls.PlayerOneBlock]) {
        firstFighter.lastBlockTime = performance.now();
      }
      if (keyMap[controls.PlayerTwoAttack]) {
        if (!secondFighter.isBlocking)
          if (attack(secondFighter, firstFighter))
            updateHealthBar(firstFighter, 'right');
      }
      if (keyMap[controls.PlayerTwoCriticalHitCombination[0]]
        && keyMap[controls.PlayerTwoCriticalHitCombination[1]]
        && keyMap[controls.PlayerTwoCriticalHitCombination[2]]) {
        if (!secondFighter.isBlocking)
          if (superAttack(secondFighter, firstFighter))
            updateHealthBar(firstFighter, 'right');
      }
      if (keyMap[controls.PlayerTwoBlock]) {
        secondFighter.lastBlockTime = performance.now();
      }
  
      if (firstFighter.health < 1) {
        resolve(secondFighter);
        window.removeEventListener("keydown", processFight);
        window.removeEventListener("keyup", processFight);
        return;
      }
      if (secondFighter.health < 1) {
        resolve(firstFighter);
        window.removeEventListener("keydown", processFight);
        window.removeEventListener("keyup", processFight);
        return;
      }
    };
    window.addEventListener("keydown", processFight);
    window.addEventListener("keyup", processFight);
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const dmg = getHitPower(attacker) - getBlockPower(defender);
  if (dmg > 0)
    return dmg;
  else
    return 0;
}

export function getHitPower(fighter) {
  // return hit power
  return fighter.attack * getChance(1, 2);
}

export function getBlockPower(fighter) {
  // return block power
  if (performance.now() - fighter.lastBlockTime < 50)
    return fighter.defense * getChance(1, 2); 
  return 0;
}

function getChance(min, max) {
  return Math.random() * (max - min) + min;
}

function attack(attacker, defender) {
  if (!attacker.attackCooldown) {
    defender.health -= getDamage(attacker, defender);
    attacker.attackCooldown = true;
    setTimeout(() => { attacker.attackCooldown = false }, 1000);
    console.log(`Defender has ${defender.health}HP`)
    return true;
  }
  return false;
}

function superAttack(attacker, defender) {
  if (!attacker.attackCooldown && !attacker.superAttackCooldown) {
    defender.health -= attacker.attack * 2;
    attacker.attackCooldown = true;
    attacker.superAttackCooldown = true;
    setTimeout(() => { attacker.attackCooldown = false }, 1000);
    setTimeout(() => { attacker.superAttackCooldown = false }, 10000);
    console.log(`Defender has ${defender.health}HP`)
    return true;
  }
  return false;
}

async function updateHealthBar(fighter, position) {
  let healthBar = document.getElementById(`${position}-fighter-indicator`);
  healthBar.style.width = `${(fighter.health / fighter.originalHealth) * 100}%`
}