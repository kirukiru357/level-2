import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  console.log(fighter);
  // todo: show fighter info (image, name, health, etc.)
  const fighterImage = createFighterImage({
    source: fighter.source,
    name: fighter.name,
  });
  fighterImage.style.maxHeight = '280px';

  const fighterName = createElement({
    tagName: 'h1',
    className: 'arena___fighter-name',
  });
  fighterName.textContent = fighter.name;

  const fighterStats = createElement({
    tagName: 'div',
    className: 'arena___fighter-name',
  });
  fighterStats.innerHTML = `<p>Health: ${fighter.health}</p><p>Attack: ${fighter.attack}</p><p>Defense: ${fighter.defense}</p>`;

  fighterElement.append(fighterImage);
  fighterElement.append(fighterName);
  fighterElement.append(fighterStats);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
